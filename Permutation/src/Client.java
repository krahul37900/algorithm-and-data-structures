/**
 * 
 * Java Program to find the permutation of a given string.
 * 
 * @author rahul kulkarni
 *
 */

public class Client {

	public static void main(String[] args) {

		String s = "123";
		permute(s.toCharArray(), 0, s.length()-1);
	}

	/**
	 * Print the permutation of the given char array
	 * @param charArray
	 * @param i
	 * @param j
	 */
	private static void permute(char[] charArray, int i, int j) {
		//System.out.println("i = " + i + "j = " + j);
		
		if(i >= j)
		{
			//System.out.println("Inside exit condition");
			for(int index = 0; index < charArray.length; ++index)
				System.out.print(charArray[index]);
			System.out.println();
			return;
		} else {
			for(int index = i; index <= j; ++index)
			{
				swap(charArray, i, index);
				permute(charArray, i+1, j);
				swap(charArray, i, index);
			}
		}
	}

	/**
	 * Swap characters at i-th and j-th position
	 * @param charArray
	 * @param i
	 * @param j
	 */
	private static void swap(char[] c, int i, int j) {
		char temp = c[i];
		c[i] = c[j];
		c[j] = temp;
	}

}
